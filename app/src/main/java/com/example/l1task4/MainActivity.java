package com.example.l1task4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    EditText editText;
    Button button;
    int firstNumber;
    int secondNumber;
    double sum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        textView.setText(R.string.text_view);

        editText = (EditText) findViewById(R.id.editText);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(calculate(editText.getText().toString()));
            }
        });

    }

    public String calculate(String str){
        String [] arrOfNumbers;
        if(str.contains("+")){
            arrOfNumbers = str.split("\\+");
            firstNumber = Integer.parseInt(arrOfNumbers[0]);
            secondNumber = Integer.parseInt(arrOfNumbers[1]);
            sum = firstNumber + secondNumber;
        }
        if(str.contains("-")){
            arrOfNumbers = str.split("-");
            firstNumber = Integer.parseInt(arrOfNumbers[0]);
            secondNumber = Integer.parseInt(arrOfNumbers[1]);
            sum = (double) firstNumber - (double) secondNumber;
        }
        if(str.contains("*")){
            arrOfNumbers = str.split("\\*");
            firstNumber = Integer.parseInt(arrOfNumbers[0]);
            secondNumber = Integer.parseInt(arrOfNumbers[1]);
            sum = firstNumber * secondNumber;
        }
        if(str.contains("/")){
            arrOfNumbers = str.split("/");
            firstNumber = Integer.parseInt(arrOfNumbers[0]);
            secondNumber = Integer.parseInt(arrOfNumbers[1]);
            sum = (double) firstNumber / (double) secondNumber;
        }
        return "" + sum;
    }
}
